$directoryName = $args[0]
[System.Collections.ArrayList]$files = @()

$raw_buffer_name = "_csv_raw.csv"
$idx_buffer_name = "_csv_idx.csv"
$final_buffer_name = "_csv_final.csv"
$buffer_name_size = $raw_buffer_name.Length
$Items = (Get-ChildItem -Path $directoryName -Filter *.csv)

for ($i = 0; $i -lt $Items.Name.Length; $i++)
{
    $name_size = $Items.Name[$i].Length
    $name = $Items.Name[$i].Substring(0, $name_size - $buffer_name_size)

    if ((-not $files.Contains($name)))
    {
        $files.Add($name)
    }
}

foreach ($i in $files)
{
    $baseName = "\" + $i
    $raw_buffer_file = $Items.DirectoryName[0] + $baseName + $raw_buffer_name
    $idx_buffer_file = $Items.DirectoryName[0] + $baseName + $idx_buffer_name
    $final_buffer_file = $directoryName + $baseName + $final_buffer_name

    python.exe .\Naraka_TransferIDXData.py $raw_buffer_file $idx_buffer_file $final_buffer_file
}
# A blender plugin tool folder

## CSV Batch Importer

![UI](/img/01.PNG)

基本界面组成如上图所示
主要由三个部分构成：
1. 导入设置
2. 导入操作
3. 确定语义

未来将支持的功能：
1. 可以将顶点颜色打包到uv中存储（针对Unity中只有一个顶点颜色的特殊功能）
2. 可以导入骨骼权重数据
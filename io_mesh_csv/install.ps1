Copy-Item -Path ./*.py -Destination E:/io_mesh_csv/ -Force
$data = @{
    Path             = 'E:/io_mesh_csv/*'
    DestinationPath  = 'E:/io_mesh_csv.zip'
    CompressionLevel = 'Fastest'
    Force            = $true
    Verbose          = $true
}
Compress-Archive @data